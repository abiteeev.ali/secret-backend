This is instruction to create db for this backend project.
1. Create new postgres db
CREATE DATABASE secret
2. Enable pgcrypto:
CREATE EXTENSION pgcrypto;
3. Create table with users:
CREATE TABLE users (
  id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  email TEXT NOT NULL UNIQUE,
  password TEXT NOT NULL
);
4. Create function to put data in table users:
CREATE OR REPLACE FUNCTION put_user(email TEXT, password TEXT)
RETURNS void 
LANGUAGE plpgsql 
AS
$$ 
BEGIN
  INSERT INTO users (email, password) VALUES (
  email,
  crypt(password, gen_salt('bf'))
  );
END;
$$;
5. Create function to check password and login of user:
CREATE OR REPLACE FUNCTION validate_user(with_email TEXT, with_password TEXT)
RETURNS BOOLEAN 
LANGUAGE plpgsql 
AS
$$ 
DECLARE
  data_exists BOOLEAN;
BEGIN
  SELECT EXISTS(SELECT * FROM users 
  WHERE email=with_email and password=crypt(with_password, password)) 
  INTO data_exists;
  RETURN data_exists;
END;
$$;S