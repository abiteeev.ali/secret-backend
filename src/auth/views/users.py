from asyncpg.exceptions import UniqueViolationError
from fastapi import APIRouter, HTTPException
from pydantic import BaseModel

from ..models.users import User

router = APIRouter()


class UserModel(BaseModel):
    email: str
    password: str


@router.post("/user", status_code=200)
async def add_user(user: UserModel):
    try:
        await User.create(email=user.email, password=user.password)
        return {"result": "New user successfully registraЫЫted"}
    except UniqueViolationError:
        raise HTTPException(status_code=409, detail="User with this email already exists")


@router.get("/user")
async def validate(user: UserModel):
    return {"result": await User.validate_user(user.email, user.password)}


class UserDeletionModel(BaseModel):
    id: int


@router.delete("/user")
async def delete_user(uid: UserDeletionModel):
    user = await User.get_or_404(uid.id)
    await user.delete(uid.id)
    return


def init_app(app):
    app.include_router(router)
