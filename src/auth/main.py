from fastapi import FastAPI
from .models import db
import logging

from .views.users import init_app as init_app

logger = logging.getLogger(__name__)


def load_app(app=None):
    if app:
        init_app(app)


def get_app():
    app = FastAPI(title="Secret")
    db.init_app(app)
    load_app(app)
    return app
