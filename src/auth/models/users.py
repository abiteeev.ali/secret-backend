from . import db


class User(db.Model):
    """GINO model of users table"""
    __tablename__ = "users"

    id = db.Column(db.Integer(), primary_key=True)
    email = db.Column(db.String())
    password = db.Column(db.String())

    @classmethod
    async def validate_user(cls, with_email: str, with_password: str) -> bool:
        """
        Validate user data to login using db dataS
        :param with_email email of user to validate
        :param with_password password of user to validate

        :return bool, result of validation
        """
        query = db.text("SELECT * FROM validate_user(:with_email, :with_password)")
        row = await db.first(query, with_email=with_email, with_password=with_password)
        return row[0]

    @classmethod
    async def put_user(cls, with_email: str, with_password: str):
        """
        Put new user in db
        :param with_email email of user to put
        :param with_password password of user to putS
        """
        query = db.text("SELECT put_user(:with_email, :with_password)")
        await db.first(query, with_email=with_email, with_password=with_password)
