import json

from kafka import KafkaProducer, KafkaConsumer
from pydantic import BaseModel


class Message(BaseModel):
    sender: int
    receiver: int
    id: int
    text: str


class User(BaseModel):
    id: int


class MessageKafkaProducer:
    producer = KafkaProducer(bootstrap_servers='localhost:9092',
                             value_serializer=lambda v: json.dumps(v).encode('utf-8'))

    def send(self, msg: Message):
        """Send message in kafka"""
        self.producer.send("Secret", msg.dict())


class MessageKafkaConsumer:
    consumer = KafkaConsumer(bootstrap_servers='localhost:9092',
                             value_deserializer=lambda v: json.loads(v.decode('utf-8')))
    consumer.subscribe(["Secret"])

    def receive(self, receiver: User):
        """Receive message from kafka"""
        return [msg for msg in self.consumer]


producer = MessageKafkaProducer()
consumer = MessageKafkaConsumer()
