from fastapi import FastAPI
import logging

from src.message.views.message import init_app

logger = logging.getLogger(__name__)


def load_app(app=None):
    if app:
        init_app(app)


def get_app():
    app = FastAPI(title="Message")
    load_app(app)
    return app
