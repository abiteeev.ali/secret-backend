from fastapi import APIRouter
from pydantic import BaseModel

from ..models.message import Message, producer, consumer
router = APIRouter()


@router.post("/message", status_code=200)
async def send_message(msg: Message):
    producer.send(msg=msg)
    return {"result": "Successfully send message"}


class User(BaseModel):
    id: int


@router.get("/message", status_code=200)
async def receive_message(usr: User):
    msgs = consumer.receive(receiver=usr)
    return {"msgs": msgs}


def init_app(app):
    app.include_router(router)
